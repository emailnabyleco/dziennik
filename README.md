DzienniczekUcznia

Opis programu:
Program powinien spełniać podstawowe funkcjonalności dzienniczka obecności ucznia. Czyli powinien zapewniać kilka funkcjonalności: 
- powinien zawierać uczniów (dane można zmockupować, bądź dodać jednorazowo poprzez mechanizm fixture)
- wyświetlać kalendarz na dany miesiąc wraz z informacją o statusie danego ucznia w danym dniu (możliwość przełączenia miesiąca)
- możliwość ustawiania oraz edytowania statusu ucznia w danym dniu (4 opcje: (puste), obecny - symbol "O", nieobecny - symbol "N", spóźnienie - symbol "S")

Wymagane technologie:
- PHP5/7 + Framework Symfony3
- Baza danych typu SQL 
- mile widziane testy (jednostkowe - PHPUnit/PHPSpec, akceptacyjne - Behat)

-------
Wszystko zrobione oprócz Behat.