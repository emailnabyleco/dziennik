<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pupil
 *
 * @ORM\Table(name="pupil", uniqueConstraints={@ORM\UniqueConstraint(name="idpupil_UNIQUE", columns={"idpupil"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PupilRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Pupil
{
    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=64, nullable=false)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=128, nullable=false)
     */
    private $lastname;

    /**
     * @var integer
     *
     * @ORM\Column(name="idpupil", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpupil;



    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Pupil
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Pupil
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Get idpupil
     *
     * @return integer
     */
    public function getIdpupil()
    {
        return $this->idpupil;
    }
}
