<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classregister
 *
 * @ORM\Table(name="classregister", 
 * uniqueConstraints={@ORM\UniqueConstraint(name="idclassregister_UNIQUE", columns={"idclassregister"})}, 
 * indexes={@ORM\Index(name="fk_classregister_status_idx", columns={"status_id"}), 
 * @ORM\Index(name="fk_classregister_pupil1_idx", columns={"pupil_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClassRegisterRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Classregister
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="idclassregister", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idclassregister;

    /**
     * @var \AppBundle\Entity\Pupil
     * 
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pupil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pupil_id", referencedColumnName="idpupil")
     * })
     */
    private $pupil;

    /**
     * @var \AppBundle\Entity\Status
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Status")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="idstatus")
     * })
     */
    private $status;



    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Classregister
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set idclassregister
     *
     * @param integer $idclassregister
     *
     * @return Classregister
     */
    public function setIdclassregister($idclassregister)
    {
        $this->idclassregister = $idclassregister;

        return $this;
    }

    /**
     * Get idclassregister
     *
     * @return integer
     */
    public function getIdclassregister()
    {
        return $this->idclassregister;
    }

    /**
     * Set pupil
     *
     * @param \AppBundle\Entity\Pupil $pupil
     *
     * @return Classregister
     */
    public function setPupil(\AppBundle\Entity\Pupil $pupil)
    {
        $this->pupil = $pupil;

        return $this;
    }

    /**
     * Get pupil
     *
     * @return \AppBundle\Entity\Pupil
     */
    public function getPupil()
    {
        return $this->pupil;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\Status $status
     *
     * @return Classregister
     */
    public function setStatus(\AppBundle\Entity\Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }
}
