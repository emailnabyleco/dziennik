<?php

namespace AppBundle\Features\Context;

use Behat\Gherkin\Node\PyStringNode;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;

class FeatureContext extends MinkContext implements KernelAwareContext
{
    private $kernel;

    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }
    

    /**
     * @When I click on link with class :arg1
     */
    public function iClickOnLinkWithClass($arg1)
    {
        // $arg1 will have my class name?
        $this->getSession()->getPage()->find('css', $arg1)->click();
        
    }

    /**
     * @Then link should have class :arg1
     */
    public function linkShouldHaveClass($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When I click on date in calendar
     */
    public function iClickOnDateInCalendar()
    {
        throw new PendingException();
    }

    /**
     * @Then I should be in :arg1 with choosen date
     */
    public function iShouldBeInWithChoosenDate($arg1)
    {
        throw new PendingException();
    }
}
