<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{

    /**
     * default method for handling DefaultController
     * 
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('calendar', array('yearMonth' => ''), 301);
    }

    /**
     * method for handling a request for showing a calendar of 
     * a given year and month in parameter yearMonth
     * 
     * @Route("/calendar/{yearMonth}",
     *          defaults = {"yearMonth" = ""}, 
     *          requirements = {"yearMonth" : "[0-9]{4}-[0-9]{2}"},
     *          name = "calendar")
     * 
     * @param string $yearMonth year and month in required format YYYY-MM
     */
    public function yearMonthAction(string $yearMonth)
    {

        /* 
            first run yearMonth is always empty by default, other times it must check if isYearMonthValid
            returns true
        */
        if (empty($yearMonth) || $this->isYearMonthValid($yearMonth) === false) {

            return $this->redirectToRoute('calendar', array('yearMonth' => date('Y-m')), 301);

        }

        /*
            preparations for rendering view
        */
        $explodedDateMonth = explode('-', $yearMonth);
        $timestamp = strtotime("01-" . $explodedDateMonth[1] . "-" . $explodedDateMonth[0]);

        /*
            for previous month button
        */
        $previousMonth = date('Y-m', strtotime($yearMonth." - 1 month"));

        /*
            for next month button
        */
        $nextMonth = date('Y-m', strtotime($yearMonth." + 1 month"));

        /*
            special method for generating array with calendar days, for details look below
        */
        $calendar = $this->genCalendar($yearMonth);

        return $this->render(
                                'default/index.html.twig',
                                [
                                    'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
                                    'calendar' => $calendar,
                                    'year' => $explodedDateMonth[0],
                                    'year_word' => date('Y', $timestamp),
                                    'month' => $explodedDateMonth[1],
                                    'month_prev' => $previousMonth,
                                    'month_next' => $nextMonth,
                                    'month_word' => date('F', $timestamp),
                                ]
                            );

    }

    /**
     * checks if parameter value has correct standard YYYY-MM 
     * which is needed for generating calendar view
     * 
     * @param string $yearMonth value for check
     * @return bool result of check
    */
    private function isYearMonthValid(string $yearMonth) : bool {

        if (strlen($yearMonth) == 7) {

            if (strcmp($yearMonth[4], '-') == 0) {

                $year = substr($yearMonth, 0, 4);
                $month = substr($yearMonth, 5, 2);
                $intMonth = intval($month);

                if ($this->onlyDigits($year) && 
                    $this->onlyDigits($month) &&
                    $intMonth > 0 &&
                    $intMonth < 13
                    ) {

                    return true;

                }

            }

        }

        return false;

    }

    /**
     * checks if input value consist only from digits
     * 
     * @param string $string parameter for check
     * @return bool
    */
    private function onlyDigits(string $string) : bool {
        
        $long = strlen($string);

        for($i = 0; $i < $long; $i++) {

            if (!is_numeric($string[$i])) {

                return false;

            }

        }

        return true;

    }

    /**
     * Generates an array which has values located like on calendar,
     * where position in array respondes to the day of the week 
     * (1st day is Monday, index zero in one dimesional array)
     * for example first week of October 2017 looks like: ["", "", "", "", "", "", "1"]
     * because 1st October 2017 was Sunday. Current day on such callendar is marked as
     * and array with single element, where element is the number of current day
     * 
     * @param yearMonth - must have format: YYYY-MM, example : 2017-10
     * @return array
    */
    public function genCalendar(string $yearMonth) : array {

        $date = explode("-", $yearMonth);
        $timestamp = strtotime('01-' . $date[1] ."-". $date[0]);
        $howManyDays = date('t', $timestamp);
        $firstDayOfMonth = date('N', $timestamp);

        $calendar = [];
        $week = [];
        $startDay = 1;

        for($i = 1, $j = 0; $i <= $howManyDays; $i++, $j++) {
            
                $timestamp = strtotime("$i-" . $date[1] . "-" . $date[0]);
                $dayOfWeek = date('N', $timestamp) - 1;

                if ($i == 1) { //check which day of week is at the first day of month

                    if ($dayOfWeek > 0) {

                        if ($dayOfWeek == 6) {
                            $j = -1;

                            for($k = 0; $k < $dayOfWeek; $k++) {
                                $week[$k] = "";
                                
                            }

                            $this->setCurrentDay($week, $dayOfWeek, $i);

                            array_push($calendar, $week);
                            $week = [];

                        } else {

                            $j = $dayOfWeek;

                            for($k = 0; $k < $dayOfWeek; $k++) {
                                $week[$k] = "";
                            }

                            $this->setCurrentDay($week, $dayOfWeek, $i);

                        }

                    } else {

                        $this->setCurrentDay($week, $dayOfWeek, $i);
                        
                    }

                } else {
                    
                    if ($j === 7) {
                        
                        $j = 0;
                        
                        array_push($calendar, $week);
                        
                        $week = [];
                        $this->setCurrentDay($week, $dayOfWeek, $i);
            
                    } else {
                        
                        $this->setCurrentDay($week, $dayOfWeek, $i);

                    }
            
                }
            
            }
        
        $this->closeTable($week);
        array_push($calendar, $week);        

        return $calendar;

    }

    /**
     * helper method for fullfiling the rest days of week with empty values
     * when generating calendar array, for example : 
     * input value : ["1", ... , "27", "29", "30"] 
     * output generated: ["1", ... , "27", "29", "30", "", "", "", ""]
     * 
     * @param array $currentMonthCalendar array generated by method genCalendar
     * @return array
    */
    private function closeTable(array &$currentMonthCalendar) {

        $howManyItems = count($currentMonthCalendar);

        for ($i = $howManyItems; $i < 7; $i++) {

            $currentMonthCalendar[$i] = "";

        }

    }

    /**
     * helper method for setting the current day in genCalendar method
     * 
     * @param array $week - current week in calendar
     * @param int $dayOfWeek - day of the week
     * @param int $calendarNumber - number of a calendad day
    */
    private function setCurrentDay(array &$week, int $dayOfWeek, int $calendarNumber) {

        $today = date('j');

        if ($calendarNumber == $today) {

            $week[$dayOfWeek] = [$calendarNumber];

        } else {

            $week[$dayOfWeek] = $calendarNumber;

        }

    }

}
