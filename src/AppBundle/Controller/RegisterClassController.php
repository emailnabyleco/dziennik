<?php

namespace AppBundle\Controller;

use \DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Classregister;

class RegisterClassController extends Controller
{
    
    /**
     * default method for handling RegisterClassController
     * 
     * @Route("/classregister/{date}/{page}", 
     *          defaults = {"page" = "1"}, 
     *          requirements = { "date" : "[0-9]{4}-[0-9]{2}-[0-9]{2}", "page" : "\d+"}, 
     *          name="classregister")
     * 
     * @param string $date date in classregister
     * @param int $page which page of classregister to display
     * 
     */
    public function registerClassAction(string $date, int $page)
    {
        /* generate dates for classregister view */
        $timestamp = strtotime($date);
        $dateForm = date('d-m-Y', $timestamp);
        $dateRev = date('Y-m', $timestamp);
        $dayName = date('l', $timestamp);

        /* get data for classregister page */
        $classRegisterRepo = $this->getDoctrine()->getRepository('AppBundle:Classregister');
        $data = $classRegisterRepo->getAllPupilsByDate($date);

        /* if no classregister for given day was found then creates a new for given day */
        if (empty($data)) {

            $pupilsRepo = $this->getDoctrine()->getRepository('AppBundle:Pupil');
            $pupils = $pupilsRepo->getAllPupils();

            $this->createClass($pupils, $date);

            $classRegisterRepo = $this->getDoctrine()->getRepository('AppBundle:Classregister');
            $data = $classRegisterRepo->getAllPupilsByDate($date);

        }

        $paginator = $this->get('knp_paginator');
        $data = $paginator->paginate(
                                        $data,
                                        $page,
                                        10
                                    );

        return $this->render(
                                'classreg/classregister.html.twig',
                                [
                                    'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
                                    'date_rev' => $dateRev,
                                    'date_dmy' => $dateForm,
                                    'date_ymd' => $date,
                                    'day' => $dayName,
                                    'data' => $data,
                                    'curr_page' => $page
                                ]
                            );

    }

    /**
     * method creates a classregister for all provided pupils and sets them the
     * default status(empty)
     * 
     * @param array $pupils provided pupils from db
     * @param string $date date in classregister
    */
    public function createClass(array $pupils, string $date) {

        $em = $this->getDoctrine()->getManager();
        $status = $this->getDoctrine()->getRepository('AppBundle:Status')->getStatusByStatusColor("default");

        foreach($pupils as $pupil) {

            $newClassRegister = new ClassRegister;
            $newClassRegister->setPupil($pupil);
            $newClassRegister->setStatus($status);
            $newClassRegister->setDate(DateTime::createFromFormat('Y-m-d', $date));

            $em->persist($newClassRegister);

        }

        $em->flush();

    }

    /**
     * calls the service and perform changing status of a pupil
     * after this redirects back to given page in classregister
     * 
     * @Route("/classregister/date/{date}/page/{page}/pupil/{idPupil}/status/{statusColor}", 
     *          requirements = {"idstaus" : "\d+", "idPupil" : "\d+" }, 
     *          name = "set_pupil_classregister_status")
     */
    public function pupilStatusAction($date, $page, $idPupil, $statusColor) {
        
        /* get service, which will change pupil's status */
        $changePupilStatusService = $this->get('class_register.pupil_status');
        $changePupilStatusService->changeStatus($date, $statusColor, $idPupil);
        
        return $this->redirectToRoute('classregister', array('date' => $date, 'page' => $page));

    }

}
