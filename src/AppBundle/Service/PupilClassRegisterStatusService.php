<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Classregister;
use AppBundle\Entity\Status;

/**
 * Service for chaning pupil's status at the classregistry page
*/
class PupilClassRegisterStatusService {

    private $em;

    public function __construct(EntityManagerInterface $em) {

        $this->em = $em;

    }

    /**
     * change status for the given pupil's id at the choosen day for new status if older status
     * is different then new
     * 
     * @param string $date date of a classregister
     * @param string $statusColor new status of pupil
     * @param int $idUser id of a pupil
     * @return bool
    */
    public function changeStatus(string $date, string $statusColor, int $idUser) : bool {
        
        $newStatus = $this->em->getRepository('AppBundle:Status')->getStatusByStatusColor($statusColor);
        $classRegister = $this->em->getRepository('AppBundle:Classregister')->‌​findByDate($date, $idUser);
        
        if (!empty($classRegister) && !empty($newStatus)) {

            $currentStatus = $classRegister->getStatus();

            if ($currentStatus->getColor() != $newStatus->getColor()) {

                $classRegister->setStatus($newStatus);
                $this->em->merge($classRegister);
                $this->em->flush();

                return true;

            }

        }

        return false;
        
    }

}

?>