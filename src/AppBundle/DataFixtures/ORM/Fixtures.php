<?php

namespace AppBundle\DataFixtures\ORM;

use \Datetime;
use AppBundle\Entity\Pupil;
use AppBundle\Entity\Classregister;
use AppBundle\Entity\Status;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{
    /**
     * how many records to generate
    */
    const RECORDS = 32;

    /**
     * all available statuses
     * 
     * @var array
    */
    private $statuses = ['obecny' => 'success', 'spóźnienie' => 'warning', 
                        'nieobecny' => 'danger', 'pusty' => 'default'];

    /**
     * @var Doctrine\Common\Persistence\ObjectManager
    */
    private $manager;

    public function load(ObjectManager $manager)
    {

        $this->manager = $manager;

        for ($i = 0; $i < self::RECORDS; $i++) {

            $classReg = new Classregister();
            
            $classReg->setPupil($this->genPupil());
            $classReg->setStatus($this->genStatus());
            $classReg->setDate(DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
            $manager->persist($classReg);

        }

        $manager->flush();

    }

    /**
     * method for creating statuses in db and for random returning one
     * 
     * @return AppBundle\Entity\Status
    */
    private function genStatus() : Status
    {

        $stats = $this->manager->getRepository('AppBundle:Status')->getAllStatusNames();
        
        if ($stats->isEmpty() ) {

            foreach($this->statuses as $statusName => $statusColor) {

                $stat = new Status;
                $stat->setColor($statusColor);
                $stat->setName($statusName);

                $this->manager->persist($stat);

            }

            $this->manager->flush();

            $stats = $this->manager->getRepository('AppBundle:Status')->getAllStatusNames();

        } 

        return $stats->get(mt_rand(0, count($this->statuses) - 1));

    }

    /**
     * method for creating pupils in db and for random returning one
     * 
     * @return AppBundle\Entity\Pupil
    */
    private function genPupil() : Pupil
    {

        $names = [
                    'Jan', 'Filip', 'Franciszek', 'Mikołaj', 'Aleksander', 'Kacper', 'Wojciech',
                    'Maja', 'Hanna', 'Zofia', 'Amelia', 'Alicja', 'Aleksandra', 'Natalia'
                    ];
        
        $surnames = [
                        'Nowak', 'Kowalski', 'Wiśniewski', 'Wójcik', 'Kowalczyk', 'Kamiński', 'Lewandowski', 
                        'Dąbrowski', 'Zieliński', 'Szymański', 'Woźniak', 'Kozłowski', 'Jankowski', 'Mazur',
                        'Wojciechowski', 'Kwiatkowski', 'Krawczyk', 'Piotrowski'
                        ];

        $pupil = new Pupil();
        $pupil->setFirstname($names[mt_rand(0, count($names) - 1)]);
        $pupil->setLastname($surnames[mt_rand(0, count($surnames) - 1)]);
        
        $this->manager->persist($pupil);
        $this->manager->flush();

        return $pupil;

    }

}

?>