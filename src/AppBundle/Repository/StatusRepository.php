<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections;

class StatusRepository extends EntityRepository
{

    /**
     * gets all statuses
     * 
     * @return ArrayCollection
    */
    public function getAllStatusNames()
    {
        $qb = $this->createQueryBuilder('s');
        $qb->select('s');
        
        $result = $qb->getQuery()->getResult();

        return new Collections\ArrayCollection($result);

    }

    /**
     * gets status by the color
     * 
     * @param string $color color name of status
     * @return mixed null or object of class Status
    */
    public function getStatusByStatusColor(string $color) {

        $status = null;

        $qb = $this->createQueryBuilder('s');
        
        $qb->select('s');
        $qb->where('s.color = :color');
        $qb->setParameter('color', $color);

        try{

            $status = $qb->getQuery()->getSingleResult();

        } catch(NonUniqueResultException $ex) {

            echo 'Problem with Query in StatusRepository->getStatusByStatusColor : ',  $ex->getMessage(), "\n";

        }

        return $status;

    }

}

?>