<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PupilRepository extends EntityRepository
{

    /**
     * gets all pupil in orber by last name, first name
     * @return array
    */
    public function getAllPupils()
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('p');
        $qb->addOrderBy('p.lastname', 'ASC');
        $qb->addOrderBy('p.firstname', 'ASC');
        
        return $qb->getQuery()->getResult();

    }

    /**
     * gets pupil by it's id
     * 
     * @param int $id pupil's id
     * @return mixed null or object of Pupil class
    */
    public function getPupilById(int $id) {

        $pupil = null;

        $qb = $this->createQueryBuilder('p');
        
        $qb->select('p');
        $qb->where('p.idpupil = :idpupil');
        $qb->setParameter('idpupil', $id);

        try {

            $pupil = $qb->getQuery()->getSingleResult();

        } catch(NonUniqueResultException $ex) {
            
            echo 'Problem with Query in PupilRepository->getPupilById : ',  $ex->getMessage(), "\n";

        }

        return $pupil;

    }

}

?>