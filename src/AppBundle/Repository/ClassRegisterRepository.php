<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

class ClassRegisterRepository extends EntityRepository
{
    /**
     * method returns all pupil connected with a classregister for given day of a classregister
     * 
     * @param string $classRegisterDate date in format YYYY-MM-DD
     * @return array selected classregister data
    */
    public function getAllPupilsByDate(string $classRegisterDate)
    {
        
        $qb = $this->createQueryBuilder('c');

        $qb->select('c', 'pupil', 'status');
        $qb->join('c.pupil', 'pupil');
        $qb->join('c.status', 'status');
        $qb->where('c.date = :date');
        $qb->setParameter('date', $classRegisterDate);
        $qb->addOrderBy('pupil.lastname', 'ASC');
        $qb->addOrderBy('pupil.firstname', 'ASC');

        return $qb->getQuery()->getResult();

    }

    /**
     * method returns classregister for given date and id of pupil
     * 
     * @param string $date date of a classregister
     * @param int $idpupil id of a pupil
     * @return mixed null if not found or object of Classregister
    */
    public function ‌​findByDate(string $date, int $idpupil) {

        $queryResult = null;

        $qb = $this->createQueryBuilder('c');
        
        $qb->select('c', 'pupil');
        $qb->join('c.pupil', 'pupil');
        $qb->where('c.date = :date', 'pupil.idpupil = :idpupil');
        $qb->setParameter('date', $date);
        $qb->setParameter('idpupil', $idpupil);

        try {

            $queryResult = $qb->getQuery()->getOneOrNullResult();

        } catch(NonUniqueResultException $ex) {

            echo 'Problem with Query in ‌ClassRegisterRepository->​findByDate : ',  $ex->getMessage(), "\n";

        }

        return $queryResult;

    }

}

?>