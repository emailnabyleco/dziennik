<?php

namespace Tests\AppBundle\Repository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BaseRepository extends KernelTestCase {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    
    protected function setUp()
    {

        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
                            ->get('doctrine')
                            ->getManager();

    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null;

    }
   
}

?>