<?php

namespace Tests\AppBundle\Repository;

use Tests\AppBundle\Repository\BaseRepository;
use AppBundle\Entity\Status;

class StatusRepositoryTest extends BaseRepository {

    public function testGetStatusByStatusColorUsingGoodColor() {

        $status = $this->em->getRepository('AppBundle:Status')->getStatusByStatusColor("success");
        $this->assertInstanceOf(Status::class, $status);

    }

    public function testGetStatusByStatusColorUsingBadColor() {

        $this->setExpectedException('Doctrine\ORM\NoResultException');
        $status = $this->em->getRepository('AppBundle:Status')->getStatusByStatusColor("none");

    }

    public function testGetAllStatusNames() {

        $statuses = $this->em->getRepository('AppBundle:Status')->getAllStatusNames();
        $this->assertNotEmpty($statuses);

    }

}

?>