<?php

namespace Tests\AppBundle\Repository;

use Tests\AppBundle\Repository\BaseRepository;

class PupilRepositoryTest extends BaseRepository {


    public function testGetAllPupils()
    {
        
        $pupils = $this->em->getRepository('AppBundle:Pupil')->getAllPupils();
        $this->assertNotEmpty($pupils);

    }

    public function pupilsIdSource() {

        return [
                [0]
                ];

    }

    /**
     * @dataProvider pupilsIdSource
    */
    public function testGetPupilByIdUsingInvalideId(int $idPupil)
    {
        
        $this->setExpectedException('Doctrine\ORM\NoResultException');
        $pupil = $this->em->getRepository('AppBundle:Pupil')->getPupilById($idPupil);

    }

}

?>