<?php

namespace Tests\AppBundle\Repository;

use Tests\AppBundle\Repository\BaseRepository;

class ClassRegisterRepositoryTest extends BaseRepository {

    public function testGetAllPupilsByDateUsingInvalidDate() {
        
        $classregister = $this->em->getRepository('AppBundle:Classregister')->getAllPupilsByDate("2000-01-01");
        $this->assertEmpty($classregister);

    }

    public function testFindByDateUsingInvalideData() {

        $classregister = $this->em->getRepository('AppBundle:Classregister')->‌​findByDate("2000-01-01", 1);
        $this->assertNull($classregister);

    }

}

?>