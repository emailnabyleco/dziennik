<?php

namespace Tests\AppBundle\Service;

use PHPUnit\Framework\TestCase;
use AppBundle\Service\PupilClassRegisterStatusService;
use Doctrine\ORM\EntityRepository;

class PupilClassRegisterStatusServiceTest extends TestCase {

    /**
     * @var EntityManager|\PHPUnit_Framework_MockObject_MockObject
    */
    protected $emMock;

    protected $repoStatusMock;

    protected $repoClassregMock;

    protected function setUp()
    {

        $this->emMock = $this->getMockBuilder('Doctrine\ORM\EntityManager')
                            ->disableOriginalConstructor()
                            ->setMethods(['merge', 'flush', 'getRepository'])
                            ->getMock();

        $this->repoStatusMock = $this->getMockBuilder(StatusRepository::class)
                            ->disableOriginalConstructor()
                            ->setMethods(['getStatusByStatusColor'])
                            ->getMock();

        $this->repoClassregMock = $this->getMockBuilder(ClassRegisterRepository::class)
                            ->disableOriginalConstructor()
                            ->setMethods(['‌​findByDate'])
                            ->getMock();

    }

    public function testPupilClassRegisterStatusReturnsFalse()
    {

        $this->emMock->expects($this->at(0))->method('getRepository')->with('AppBundle:Status')->willReturn($this->repoStatusMock);
        $this->repoStatusMock->method('getStatusByStatusColor')->willReturn('');
        
        $this->emMock->expects($this->at(1))->method('getRepository')->with('AppBundle:Classregister')->willReturn($this->repoClassregMock);
        $this->repoClassregMock->method('‌​findByDate')->willReturn('');

        $service = new PupilClassRegisterStatusService($this->emMock);
        $this->assertFalse($service->changeStatus('', '', 1));

    }

    public function testPupilClassRegisterStatusReturnsTrue()
    {
        $statusMock = $this->getMockBuilder(Status::class)
                            ->disableOriginalConstructor()
                            ->setMethods(['getColor'])
                            ->getMock();

        $classregisterMock = $this->getMockBuilder(Classregister::class)
                                ->disableOriginalConstructor()
                                ->setMethods(['setStatus', 'getStatus'])
                                ->getMock();

        $this->emMock->expects($this->at(0))->method('getRepository')->with('AppBundle:Status')->willReturn($this->repoStatusMock);
        $this->repoStatusMock->method('getStatusByStatusColor')->willReturn($statusMock);
        $statusMock->expects($this->at(0))->method('getColor')->willReturn('warning');
        
        $this->emMock->expects($this->at(1))->method('getRepository')->with('AppBundle:Classregister')->willReturn($this->repoClassregMock);
        $this->repoClassregMock->method('‌​findByDate')->willReturn($classregisterMock);
        $classregisterMock->method('getStatus')->willReturn($statusMock);
        $statusMock->expects($this->at(1))->method('getColor')->willReturn('success');
        
        $classregisterMock->method('setStatus')->willReturn('');

        $this->emMock->method('merge')->with($classregisterMock)->willReturn('');
        $this->emMock->method('flush')->willReturn('');

        $service = new PupilClassRegisterStatusService($this->emMock);
        $this->assertTrue($service->changeStatus('', '', 1));

    }

}

?>