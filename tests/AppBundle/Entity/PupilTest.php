<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Pupil;
use PHPUnit\Framework\TestCase;

class PupilTest extends TestCase {

    public function pupilData() {

        return [
                    ['john', 'great']
                ];

    }

    /**
     * @dataProvider pupilData
     * @param string $firstname name of pupil
     * @param string $lastname surname of pupil
     */
    public function testPupil($firstname, $lastname) {

        $pupil = new Pupil();
        $pupil->setFirstname($firstname);
        $pupil->setLastname($lastname);

        $this->assertEquals('john', $pupil->getFirstname());
        $this->assertEquals('great', $pupil->getLastname());
        $this->assertEmpty($pupil->getIdpupil());

    }

}

?>