<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Status;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase {

    public function statusData() {

        return [
                    ['obecny', 'success']
                ];
                
    }

    /**
     * @dataProvider statusData
     * @param string $name name of classregister status
     * @param string $color color of classregister status
     */
    public function testStatus($name, $color) {

        $status = new Status();
        $status->setName($name);
        $status->setColor($color);

        $this->assertEquals('obecny', $status->getName());
        $this->assertEquals('success', $status->getColor());
        $this->assertEmpty($status->getIdstatus());

    }

}

?>