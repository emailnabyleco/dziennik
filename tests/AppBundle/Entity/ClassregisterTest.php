<?php

namespace Tests\AppBundle\Entity;

use \DateTime;
use AppBundle\Entity\Classregister;
use AppBundle\Entity\Pupil;
use AppBundle\Entity\Status;
use PHPUnit\Framework\TestCase;

class ClassregisterTest extends TestCase {

    public function classregisterData() {

        $pupil = new Pupil();
        $pupil->setFirstname('Dawid');
        $pupil->setLastname('Sz');

        $status = new Status();
        $status->setName('obecny');
        $status->setColor('success');

        $date = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));

        return [
                    [1, $pupil, $status, $date]
                ];

    }

    /**
     * @dataProvider classregisterData
     * @param int $id id of classregister
     * @param Pupil $firstname name of pupil
     * @param Status $lastname surname of pupil
     * @param Datetime $date date of classregister
     */
    public function testPupil(int $id, Pupil $pupil, Status $status, DateTime $date) {

        $classRegister = new Classregister();
        $classRegister->setDate($date);
        $classRegister->setIdclassregister($id);
        $classRegister->setPupil($pupil);
        $classRegister->setStatus($status);

        $this->assertEquals('Dawid', $classRegister->getPupil()->getFirstname());
        $this->assertEquals('Sz', $classRegister->getPupil()->getLastname());
        $this->assertEquals(1, $classRegister->getIdclassregister());
        $this->assertEquals('obecny', $classRegister->getStatus()->getName());
        $this->assertEquals('success', $classRegister->getStatus()->getColor());
        $this->assertEquals(date('Y-m-d'), $classRegister->getDate()->format('Y-m-d'));

    }

}

?>