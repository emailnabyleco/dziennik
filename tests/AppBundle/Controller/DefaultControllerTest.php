<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(301, $client->getResponse()->getStatusCode());

    }

    public function testCalendarWithoutData() {

        $client = static::createClient();
        $crawler = $client->request('GET', '/calendar');
        
        $this->assertEquals(301, $client->getResponse()->getStatusCode());

    }

    public function validYearMonthTags() {
        return [
                    ['2017-09', '2017-10', '2017-11']
                ];
    }

    /**
     * @dataProvider validYearMonthTags
     * @param string $monthYear tag which consists of year and month 
     * in such format: YYYY-MM
     */
    public function testValidYearMonth(string $monthYear) {

        $client = static::createClient();
        
        $crawler = $client->request('GET', "/calendar/$monthYear");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Calendar', $crawler->filter('title')->text());

    }

    public function invalidYearMonthTags() {
        return [
                    ['', '200-00', '2222-22', '20OO-0a', '20001020']
                ];
    }

    /**
     * @dataProvider invalidYearMonthTags
     * @param string $monthYear tag which consists of year and month 
     * in such format: YYYY-MM
     */
    public function testInvalidYearMonth(string $monthYear) {
        
        $client = static::createClient();
        
        $crawler = $client->request('GET', "/calendar/$monthYear");

        $this->assertEquals(404, $client->getResponse()->getStatusCode());

    }



}
